package GPST;

#=======================================================================
# GPST.pm
# File ID: 5e0437a0-fafa-11dd-abd7-000475e441b9
#
# Character set: UTF-8
# ©opyleft 2002– Øyvind A. Holm <sunny@sunbase.org>
# License: GNU General Public License, see end of file for legal stuff.
#=======================================================================

use strict;
use warnings;

use GPSTdebug;
use GPSTgeo;

BEGIN {
    use Exporter ();
    our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

    @ISA = qw(Exporter);
    @EXPORT = qw(&trackpoint &postgresql_copy_safe &num_expand);
    %EXPORT_TAGS = ();
}
our @EXPORT_OK;

our $Spc = " ";

sub trackpoint {
    # Receive a hash and return a trackpoint as a string {{{
    my %Dat = @_;

    defined($Dat{'what'}) || return(undef);
    defined($Dat{'format'}) || return(undef);
    defined($Dat{'error'}) || return(undef);

    defined($Dat{'year'}) || ($Dat{'year'} = 0);
    defined($Dat{'month'}) || ($Dat{'month'} = 0);
    defined($Dat{'day'}) || ($Dat{'day'} = 0);
    defined($Dat{'hour'}) || ($Dat{'hour'} = "");
    defined($Dat{'min'}) || ($Dat{'min'} = "");
    defined($Dat{'sec'}) || ($Dat{'sec'} = "");
    $Dat{'print_time'} = (
        !$Dat{'year'} ||
        !$Dat{'month'} ||
        !$Dat{'day'} ||
        !length($Dat{'hour'}) ||
        !length($Dat{'min'}) ||
        !length($Dat{'sec'})
    ) ? 0 : 1;

    if (
        ("$Dat{'year'}$Dat{'month'}$Dat{'day'}$Dat{'hour'}$Dat{'min'}" =~
        /[^\d]/) || ($Dat{'sec'} =~ /[^\d\.]/)
    ) {
        ($Dat{'print_time'} = 0);
    }
    $Dat{'lat'} =~ /[^\d\.\-\+]/ && return(undef);
    $Dat{'lon'} =~ /[^\d\.\-\+]/ && return(undef);
    length($Dat{'ele'}) && $Dat{'ele'} =~ /[^\d\.\-\+]/ && return(undef);

    defined($Dat{'lat'}) || ($Dat{'lat'} = "");
    defined($Dat{'lon'}) || ($Dat{'lon'} = "");
    defined($Dat{'ele'}) || ($Dat{'ele'} = "");
    defined($Dat{'desc'}) || ($Dat{'desc'} = "");

    $Dat{'lat'} = num_expand($Dat{'lat'});
    $Dat{'lon'} = num_expand($Dat{'lon'});
    $Dat{'ele'} = num_expand($Dat{'ele'});

    my $Retval = "";

    if ($Dat{'what'} eq "tp") {
        if ($Dat{'format'} eq "gpsml") {
            $Retval .= gen_gpsml_entry(%Dat);
        } elsif($Dat{'format'} eq "gpx") {
            $Retval .= gen_gpx_entry(%Dat);
        } elsif($Dat{'format'} eq "clean") {
            $Retval .= "$Dat{'lon'}\t$Dat{'lat'}\t$Dat{'ele'}\n";
        } elsif($Dat{'format'} eq "xgraph") {
            if (length($Dat{'lat'}) && length($Dat{'lon'})) {
                $Retval .= "$Dat{'lon'} $Dat{'lat'}\n";
            }
        } elsif ($Dat{'format'} eq "pgtab") {
            $Retval .= gen_pgtab_entry(%Dat);
        } elsif ($Dat{'format'} eq "gpstrans") {
            $Retval .= gen_gpstrans_entry(%Dat);
        } else {
            $Retval = undef;
        }
    } else {
        $Retval = undef;
    }
    return $Retval;
    # }}}
} # trackpoint()

sub gen_gpx_entry {
    # {{{
    my %Dat = @_;
    my $Retval = "";
    $Dat{'lat'} = num_expand($Dat{'lat'});
    $Dat{'lon'} = num_expand($Dat{'lon'});
    $Dat{'ele'} = num_expand($Dat{'ele'});
    my $err_str = length($Dat{'error'}) ? $Dat{'error'} : "";
    my $lat_str = length($Dat{'lat'}) ? " lat=\"$Dat{'lat'}\"" : "";
    my $lon_str = length($Dat{'lon'}) ? " lon=\"$Dat{'lon'}\"" : "";
    my ($estr_begin, $estr_ext, $estr_end) =
       (         "",        "",        "");
    if (length($err_str)) {
        $estr_begin = "<!-- ";
        $estr_ext = "<extensions>$Spc<error>$err_str</error>$Spc</extensions>$Spc";
        $estr_end = " -->";
    }
    if (length("$lat_str$lon_str$Dat{'ele'}")) {
        $Retval .=
        join("",
            "$Spc$Spc$Spc$Spc$Spc$Spc",
            $estr_begin,
            "<trkpt$lat_str$lon_str>",
            "$Spc",
            length($Dat{'ele'})
                ? "<ele>$Dat{'ele'}</ele>$Spc"
                : "",
            $Dat{'print_time'}
                ? "<time>" .
                  "$Dat{'year'}-$Dat{'month'}-$Dat{'day'}T" .
                  "$Dat{'hour'}:$Dat{'min'}:$Dat{'sec'}Z" .
                  "</time>$Spc"
                : "",
                $estr_ext,
            "</trkpt>$estr_end\n"
        );
    }
    return($Retval);
    # }}}
} # gen_gpx_entry()

sub gen_gpsml_entry {
    # {{{
    my %Dat = @_;
    my $err_str = length($Dat{'error'}) ? $Dat{'error'} : "";
    my $Elem = length($err_str) ? "etp" : "tp";
    my $Retval = join("",
            $Dat{'print_time'}
                ? sprintf("<time>%04u-%02u-%02uT" .
                          "%02u:%02u:%02gZ</time> ",
                           $Dat{'year'}, $Dat{'month'}, $Dat{'day'},
                           $Dat{'hour'}, $Dat{'min'}, $Dat{'sec'}*1.0
                  )
                : "",
            (length($Dat{'lat'}))
                ? "<lat>" . $Dat{'lat'}*1.0 . "</lat> "
                : "",
            (length($Dat{'lon'}))
                ? "<lon>" . $Dat{'lon'}*1.0 . "</lon> "
                : "",
            (length($Dat{'ele'}))
                ? "<ele>" . $Dat{'ele'}*1.0 . "</ele> "
                : "",
            (length($Dat{'desc'}))
                ? sprintf("<desc>%s</desc> ",
                          $Dat{'desc'})
                : ""
    );
    length($Retval) &&
        ($Retval = sprintf("<%s%s> %s</%s>\n",
                           $Elem,
                           length($err_str) ? " err=\"$err_str\"" : "",
                           $Retval,
                           $Elem)
    );
    return($Retval);
    # }}}
} # gen_gpsml_entry()

sub gen_pgtab_entry {
    # {{{
    my %Dat = @_;
    my $Retval = join("\t",
        $Dat{'year'}
            ? "$Dat{'year'}-$Dat{'month'}-$Dat{'day'}T" .
              "$Dat{'hour'}:$Dat{'min'}:$Dat{'sec'}Z"
            : '\N', # date
        (length($Dat{'lat'}) && length($Dat{'lon'}))
            ? "($Dat{'lat'},$Dat{'lon'})"
            : '\N', # coor
        length($Dat{'ele'}) ? $Dat{'ele'} : '\N', # ele
        '\N', # name
        '\N', # dist
        '\N'  # description
    ) . "\n";
    return($Retval);
    # }}}
} # gen_pgtab_entry()

sub gen_gpstrans_entry {
    # {{{
    my %Dat = @_;
    my $Retval;
    my ($gpt_lat, $gpt_lon) =
       (ddd_to_dms($Dat{'lat'}), ddd_to_dms($Dat{'lon'}));
    if ($Dat{'print_time'}) {
        $Retval = "T\t$Dat{'month'}/$Dat{'day'}/$Dat{'year'} " .
                  "$Dat{'hour'}:$Dat{'min'}:$Dat{'sec'}\t" .
                  "$gpt_lat\t$gpt_lon\n";
    } else {
        $Retval = "T\t00/00/00 00:00:00\t$gpt_lat\t$gpt_lon\n";
    }
    return($Retval);
    # }}}
} # gen_gpstrans_entry()

sub postgresql_copy_safe {
    # {{{
    my $Str = shift;
    $Str =~ s/\\/\\\\/gs;
    $Str =~ s/\n/\\n/gs;
    $Str =~ s/\r/\\r/gs;
    $Str =~ s/\t/\\t/gs;
    return($Str);
    # }}}
} # postgresql_copy_safe()

sub num_expand {
    # Convert scientific notation to decimal notation {{{
    my $Retval = shift;
    length($Retval) || return("");
    if ($Retval =~ /^(.*)e([-+]?)(.*)$/) {
        my ($num, $sign, $exp) = ($1, $2, $3);
        my $sig = $sign eq '-' ? "." . ($exp - 1 + length $num) : '';
        $Retval = sprintf("%${sig}f", $Retval);
    }
    $Retval =~ s/^\+//;
    my $minus = ($Retval =~ s/^-//) ? "-" : "";
    if ($Retval =~ /\.\d/) {
        $Retval =~ s/0+$//;
        $Retval =~ s/^0+/0/;
        $Retval =~ s/^0([1-9]+)\./$1./;
        $Retval =~ s/\.$//;
    } else {
        $Retval =~ s/^0+//;
    }
    length($Retval) || ($Retval = 0);
    $Retval = $Retval ? "$minus$Retval" : 0;
    return($Retval);
    # }}}
} # num_expand()

1;
