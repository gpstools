-- hours.sql
-- Create table with position for every hour
-- File ID: 506d4180-586e-11e5-8baa-fefdb24f8e10

BEGIN;

    \echo
    \echo ================ Update hours ================

    DROP TABLE IF EXISTS hours;
    CREATE TABLE hours (
        date timestamp,
        coor point,
        name text,
        dist numeric(8,5)
    );

    INSERT INTO hours (date) VALUES (
        generate_series(
            date_trunc(
                'hour',
                (SELECT min(date) + '1 hour' FROM logg)
            ),
            (SELECT max(date) FROM logg),
            '1 hour'
        )
    );

    UPDATE hours SET coor = findpos(date);
    UPDATE hours SET coor = point(
        round(coor[0]::numeric, 6),
        round(coor[1]::numeric, 6)
    );
    UPDATE hours SET name = clname(coor), dist = cldist(coor);

COMMIT;
