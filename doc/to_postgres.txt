til_postgres.txt
File ID: 0529a718-fafb-11dd-b55f-000475e441b9

Creating the "gps" database:

  $ cd ~/bin/src/gpstools/postgres
  $ ./init.sh

Copy new things from new tracklogs into Postgres:

  1: chdir to the place where the new files are.
  1: Run "svn update" and delete all files already in the system.
  1: addpoints *.gpx

  2: cd ~/gps/div/postgres
  2: psql gps
  2: \i update_things.sql

  And then there’s the place names:

  1: cd ~/gps/poi/trans
  1: svn update, etc
  1: gpst -o pgwupd *.gpx | psql -X -a gps
  1: cd ~/gps/unit
  1: svn update, etc
  1: gpst -o pgwupd wpdata.gpx | psql -X -a gps

To add new waypoints:

Execute

  $ addpoints -t waypoint -v *.gpx
  $ psql gps
  gps=# SELECT loop_wayp_new();

To clean up the waypoints:

cd ~/bin/src/gpstools/postgres
psql gps
\i cleanup.sql

To synchronise waypoints and POI:

Postgres is necessary. You need a database called "gps" and a table called 
"tmpwayp", which is taken care of by ../postgres/create_table.sql . After that, 
execute ../poisync and edit the .gpx files.

A useful macro to use inside vimdiff when wpdata.gpx is in the left window and 
the receiving file is in the right, is:

:map <f3> ?<wpt <cr>V/<\/wpt><cr>y<C-w><C-w>GP:%!poiformat<cr>:diffu<cr><C-w><C-w><C-o>
:map <f3> V/<\/wpt><cr>y<C-w><C-w>GP:%!poiformat<cr>:diffu<cr><C-w><C-w><C-o>

(Press F1 while between <wpt></wpt>.)

vim: set ts=2 sts=2 sw=2 et :
